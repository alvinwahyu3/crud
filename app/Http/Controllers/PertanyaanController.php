<?php

namespace App\Http\Controllers;

use App\Models\Pertanyaan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listPertanyaan = Pertanyaan::all();
        return view('pertanyaan')->with('listPertanyaan', $listPertanyaan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pertanyaan_baru');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        DB::table('pertanyaan')->insert([
            'judul' => $request->judul,
            'isi' => $request->isi,
            'tanggal_dibuat' => date('Y-m-d'),
            'tanggal_diperbaharui' => date('Y-m-d'),
            'profil_id' => 1
        ]);

        $listPertanyaan = Pertanyaan::all();
        return redirect('pertanyaan')->with('listPertanyaan', $listPertanyaan);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['pertanyaan'] = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->get();
        return view('pertanyaan_detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['edit'] = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->get();

        return view('pertanyaan_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->judul = $request->input('judul');
        $pertanyaan->isi = $request->input('isi');
        $pertanyaan->tanggal_diperbaharui = date('Y-m-d');
        $pertanyaan->save();

        $data['pertanyaan'] = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->get();
        return view('pertanyaan_detail', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('pertanyaan')
            ->where('id', $id)
            ->delete();

        $listPertanyaan = Pertanyaan::all();
        return view('pertanyaan')->with('listPertanyaan', $listPertanyaan);
    }
}
