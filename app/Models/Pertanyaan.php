<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;

    protected $table='pertanyaan';

    protected $primaryKey='id';

    protected $fillable = [
        'judul',
        'isi',
        'tanggal_dibuat',
        'tanggal_diperbaharui',
        'profil_id'
    ];

    public $timestamps = false;
}
