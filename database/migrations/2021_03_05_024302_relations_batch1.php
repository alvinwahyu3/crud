<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RelationsBatch1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
            $table->foreign('profil_id')->references('id')->on('profils');
        });

        Schema::table('like_dislike_jawaban', function (Blueprint $table) {
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
            $table->foreign('profil_id')->references('id')->on('profils');
        });

        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->foreign('jawaban_tepat_id')->references('id')->on('jawaban');
            $table->foreign('profil_id')->references('id')->on('profils');
        });
        
        Schema::table('komentar_pertanyaan', function (Blueprint $table) {
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
            $table->foreign('profil_id')->references('id')->on('profils');
        });

        Schema::table('jawaban', function (Blueprint $table) {
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
            $table->foreign('profil_id')->references('id')->on('profils');
        });

        Schema::table('komentar_jawaban', function (Blueprint $table) {
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
            $table->foreign('profil_id')->references('id')->on('profils');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->dropForeign('pertanyaan_id');
            $table->dropForeign('profil_id');
        });

        Schema::table('like_dislike_jawaban', function (Blueprint $table) {
            $table->dropForeign('jawaban_id');
            $table->dropForeign('profil_id');
        });

        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->dropForeign('jawaban_tepat_id');
            $table->dropForeign('profil_id');
        });

        Schema::table('komentar_pertanyaan', function (Blueprint $table) {
            $table->dropForeign('pertanyaan_id');
            $table->dropForeign('profil_id');
        });

        Schema::table('jawaban', function (Blueprint $table) {
            $table->dropForeign('pertanyaan_id');
            $table->dropForeign('profil_id');
        });

        Schema::table('komentar_jawaban', function (Blueprint $table) {
            $table->dropForeign('jawaban_id');
            $table->dropForeign('profil_id');
        });
    }
}
