<nav class="p-3" style="border-bottom: 1px solid #929292; display: flex;">
    <a href="{{ route('pertanyaan') }}" style="font-size: 1.5rem; text-decoration: none; color: black">Laravel - CRUD</a>
    <a href="{{ route('pertanyaan.buat') }}" class="btn btn-primary" style="margin-left: auto; margin-right: 0; color: white">Buat Pertanyaan</a>
</nav>