@extends('layouts.master')

@section('content')
    <div class="row no-gutters ml-3 mt-3">
        <h3>List Pertanyaan</h3>
    </div>
    <div class="row no-gutters" style="display: flex; flex-wrap: wrap; justify-content: center">
        @if (count($listPertanyaan) > 0)
            @foreach ($listPertanyaan as $pertanyaan)
                <div class="card m-4" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{ $pertanyaan->judul }}</h5>
                        <p class="card-text">{{ $pertanyaan->isi }}</p>
                    </div>
                    <a href="{{ route('pertanyaan.show', $pertanyaan['id']) }}" class="btn btn-primary" style="color: white">Lihat Detail</a>
                </div>
            @endforeach
        @endif
    </div>
@endsection