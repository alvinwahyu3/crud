@extends('layouts.master')

@section('content')
    <div class="container  mt-3 p-4" style="background-color: white">
        <h3>Tambahkan Pertanyaan Baru</h3>
        <form action="{{ route('pertanyaan.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Judul</label>
                <input type="text" class="form-control" name="judul" placeholder="Judul Pertanyaan">
            </div>
                <div class="form-group">
                <label>Isi</label>
                <textarea type="text" class="form-control" name="isi" rows="3" placeholder="Isi Pertanyaan"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection