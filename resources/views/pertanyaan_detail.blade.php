@extends('layouts.master')

@section('content')
    <div class="container mt-3 p-4" style="background-color: white">
        <h3>{{ $pertanyaan[0]->judul }}</h3>
        <table class="mb-4">
            <tr class="mb-2">
                <td>Isi Pertanyaan</td>
                <td style="padding-left: 10px; padding-right: 10px;">:</td>
                <td>{{ $pertanyaan[0]->isi }}</td>
            </tr>
            <tr class="mb-2">
                <td>Tanggal Dibuat</td>
                <td style="padding-left: 10px; padding-right: 10px;">:</td>
                <td>{{ $pertanyaan[0]->tanggal_dibuat }}</td>
            </tr>
            <tr class="mb-2">
                <td>Tanggal Diperbaharui</td>
                <td style="padding-left: 10px; padding-right: 10px;">:</td>
                <td>{{ $pertanyaan[0]->tanggal_diperbaharui }}</td>
            </tr>
        </table>
        <div class="btn-group" style="display: ruby">
            <a href="{{ route('pertanyaan.edit', [$pertanyaan[0]->id]) }}" class="btn btn-warning" style="border-radius: 4px">Edit</a>
            <form action="{{ route('pertanyaan.hapus', [$pertanyaan[0]->id]) }}" method="POST">
                <input type="hidden" name="_method" value="delete" />
                @csrf
                <button class="btn btn-danger">Delete</button>
            </form>
        </div>
        {{-- <a href="{{ route('pertanyaan.hapus', [$pertanyaan[0]->id]) }}" class="btn btn-danger">Delete</a> --}}
    </div>
@endsection