@extends('layouts.master')

@section('content')
    <div class="container mt-3 p-4" style="background-color: white">
        <h3>Edit Pertanyaan</h3>
        <form action="{{ route('pertanyaan.update', [$edit[0]->id]) }}" method="post" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="form-group">
                <label>Judul</label>
                <input type="text" class="form-control" name="judul" value="{{ $edit[0]->judul }}">
            </div>
                <div class="form-group">
                <label>Isi</label>
                <textarea type="text" class="form-control" name="isi" rows="3">{{ $edit[0]->isi }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
        </form>
    </div>
@endsection