<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PertanyaanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/pertanyaan', [PertanyaanController::class, 'index'])->name('pertanyaan');
Route::get('/pertanyaan/create', [PertanyaanController::class, 'create'])->name('pertanyaan.buat');
Route::post('/pertanyaan/create', [PertanyaanController::class, 'store'])->name('pertanyaan.store');
Route::get('/pertanyaan/{pertanyaan_id}', [PertanyaanController::class, 'show'])->name('pertanyaan.show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', [PertanyaanController::class, 'edit'])->name('pertanyaan.edit');
Route::put('/pertanyaan/{pertanyaan_id}/edit', [PertanyaanController::class, 'update'])->name('pertanyaan.update');
Route::delete('/pertanyaan/{pertanyaan_id}', [PertanyaanController::class, 'destroy'])->name('pertanyaan.hapus');

// Route::get('/data-tables', function () {
//     return view('items.data_tables');
// });
